## Cookie experiments

For example, we try to set cookie through some pixel (on another domain)

Imagine we have html like this
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<img src="http://cookie.example.com:9090" alt="" width="0" height="0">

</body>
</html>
```

#### Need to add in the end of `/etc/hosts` file
```
127.0.0.2      cookie.example.com
```

<hr>

If you try to set cookie like this
```go
http.SetCookie(w, &http.Cookie{
	Name:   "user_id",
	Value:  "new-user",
	Domain: "example.com",
})
```
We will see:
![](docs/img/cookie_1.png)

<hr>

So next we try to set `SameSite` to None mode
```go
http.SetCookie(w, &http.Cookie{
	Name:   "user_id",
	Value:  "new-user",
	Domain: "example.com",
	SameSite: http.SameSiteNoneMode,
})
```
And we will see:
![](docs/img/cookie_2.png)

<hr>

So next we set `Secure: true`
```go
http.SetCookie(w, &http.Cookie{
	Name:   "user_id",
	Value:  "new-user",
	Domain: "example.com",
	SameSite: http.SameSiteNoneMode,
	Secure: true,
})
```
And we will see:
![](docs/img/cookie_3.png)

<hr>

Next step is to change http to https for url in `<img>` tag
```html
<img src="https://cookie.example.com:9090" alt="" width="0" height="0">
```

And in go server use https instead of http
```shell script
make gen-certs
```
> this will generate certs under testcerts directory

Use this files in go server
```go
log.Fatal(http.ListenAndServeTLS(
    "127.0.0.2:9090",
    "testcerts/go-server.crt",
    "testcerts/go-server.key",
    serv,
))
```

In go server console you will see
```shell script
$ go run cmd/app/main.go 
2022/04/25 21:53:03 http: TLS handshake error from 127.0.0.1:48654: remote error: tls: unknown certificate
```

Then go to `https://cookie.example.com:9090` and anyway stay on site

Then you can open our index.html and see

![](docs/img/cookie_4.png)
> Our cookie are set!