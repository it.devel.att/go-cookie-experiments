
gen-certs:
	mkdir -p testcerts
	# https://linuxize.com/post/creating-a-self-signed-ssl-certificate/
	openssl req -newkey rsa:4096 \
                -x509 \
                -sha256 \
                -days 3650 \
                -nodes \
                -out testcerts/go-server.crt \
                -keyout testcerts/go-server.key \
                -subj "/C=SI/ST=Ljubljana/L=Ljubljana/O=Security/OU=IT Department/CN=example.com"
