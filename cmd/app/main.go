package main

import (
	"log"
	"net/http"
)

type server struct {
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("user_id")
	if err != nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "user_id",
			Value:    "new-user",
			Domain:   "example.com",
			SameSite: http.SameSiteNoneMode,
			Secure:   true,
		})
	} else {
		log.Printf("%+v\n", cookie)
	}
	log.Println("Success handle request")
	w.WriteHeader(http.StatusOK)
}

func main() {
	serv := &server{}
	log.Fatal(http.ListenAndServeTLS(
		"127.0.0.2:9090",
		"testcerts/go-server.crt",
		"testcerts/go-server.key",
		serv,
	))
	//log.Fatal(http.ListenAndServe("127.0.0.2:9090", serv))
}
